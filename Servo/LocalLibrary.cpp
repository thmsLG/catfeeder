//
// LocalLibrary.cpp 
// Library C++ code
// ----------------------------------
// Developed with embedXcode 
// http://embedXcode.weebly.com
//
// Project 		Servo
//
// Created by 	Thomas Le Guillou, 13/04/2014 11:11
// 				___FULLUSERNAME___
//	
// Copyright 	© Thomas Le Guillou, 2014
// License 		<#license#>
//
// See 			LocalLibrary.cpp.h and ReadMe.txt for references
//


#include "LocalLibrary.h"

void blink(uint8_t pin, uint8_t times, uint16_t ms) {
  for (uint8_t i=0; i<times; i++) {
    digitalWrite(pin, HIGH); 
    delay(ms >> 1);               
    digitalWrite(pin, LOW);  
    delay(ms >> 1);              
  }
}

void modulatedBlink(uint8_t pin, uint8_t times, uint16_t HighMs, uint16_t LowMs) {
    for (uint8_t i=0; i<times; i++) {
        digitalWrite(pin, HIGH);
        delay(HighMs >> 1);
        digitalWrite(pin, LOW);
        delay(LowMs >> 1);
    }
}

void modulatedBlink(uint8_t pin, uint16_t times, uint32_t Highus, uint32_t Lowus) {
    for (uint16_t i=0; i<times; i++) {
        digitalWrite(pin, HIGH);
        delayMicroseconds(Highus);
        digitalWrite(pin, LOW);
        delayMicroseconds(Lowus);
    }
}
