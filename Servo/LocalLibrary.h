//
// File			LocalLibrary.h
// Brief		Library header
//
// Project	 	Servo
// Developed with [embedXcode](http://embedXcode.weebly.com)
// 
// Author		Thomas Le Guillou
// 				___FULLUSERNAME___
// Date			13/04/2014 11:11
// Version		<#version#>
// 
// Copyright	© Thomas Le Guillou, 2014
// License		<#license#>
//
// See			ReadMe.txt for references
//


// Core library - IDE-based
#if defined(WIRING) // Wiring specific
#include "Wiring.h"
#elif defined(MAPLE_IDE) // Maple specific
#include "WProgram.h"
#elif defined(MPIDE) // chipKIT specific
#include "WProgram.h"
#elif defined(DIGISPARK) // Digispark specific
#include "Arduino.h"
#elif defined(ENERGIA) // LaunchPad MSP430 G2 and F5529, Stellaris and Tiva, Experimeter Board FR5739 specific
#include "Energia.h"
#elif defined(MICRODUINO) // Microduino specific
#include "Arduino.h"
#elif defined(TEENSYDUINO) // Teensy specific
#include "Arduino.h"
#elif defined(ARDUINO) // Arduino 1.0 and 1.5 specific
#include "Arduino.h"
#else // error
#error Platform not defined
#endif // end IDE

#ifndef Servo_LocalLibrary_h
#define Servo_LocalLibrary_h

//
// Brief	Blink a LED
// Details	LED attached to pin is light on then light off
// Total cycle duration = ms
// Parameters:
//      pin pin to which the LED is attached
//      times number of times
//      ms cycle duration in ms
//
void blink(uint8_t pin, uint8_t times, uint16_t ms);

void modulatedBlink(uint8_t pin, uint8_t times, uint16_t HighMs, uint16_t LowMs);
void modulatedBlink(uint8_t pin, uint16_t times, uint32_t Highus, uint32_t Lowus);


#endif
